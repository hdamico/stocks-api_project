# frozen_string_literal: true

module Api
  module V1
    class StocksController < ApplicationController
      def details
        @stocks = Stock.new.details
        render stock_data
      rescue StandardError => e
        render status_error(e)
      end

      def update
        UpdateStockValuesWorker.perform_async(stock_params)
        render status_ok
      rescue StandardError => e
        render status_error(e)
      end

      def histories
        @stocks = Stock.new.query(stock_params)
        render stock_data
      rescue StandardError => e
        render status_error(e)
      end

      def remove
        RemoveStockValuesWorker.perform_async(stock_params)
        render status_ok
      rescue StandardError => e
        render status_error(e)
      end

      private

      def status_ok
        { json: { status: 'SUCCESS', message: 'No errors' }, status: :ok }
      end

      def status_error(exception)
        { json: { status: 'ERROR', message: exception.message }, status: :bad_request }
      end

      def stock_data
        { json: { data: @stocks }, status: :ok }
      end

      def stock_params
        method = obtain_method_name
        params.require(:stocks)
        if method == 'histories'
          return split_params_with_filter(params) if params.permit(:stocks, :dateFrom, :dateTo, :page, :perPage, :format)
        end
        split_params(params)
      end

      def obtain_method_name
        caller(2..2).first[/`.*'/][1..-2]
      end

      def split_params_with_filter(params)
        stocks = split_params(params)

        filters = {}
        filters[:dateTo] = params[:dateTo]
        filters[:dateFrom] = params[:dateFrom]
        filters[:page] = params[:page]
        filters[:perPage] = params[:perPage]

        { stocks: stocks, filters: filters }
      end

      def split_params(params)
        params[:stocks].tr(' ', '').split(',').map(&:upcase)
      end
    end
  end
end
