class CreateHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :histories do |t|
      t.belongs_to :stock, index: true
      t.date :date
      t.numeric :value
      t.timestamps
    end
  end
end
