# frozen_string_literal: true

class History < ApplicationRecord
  belongs_to :stock
  validates_presence_of :date, :value
end
