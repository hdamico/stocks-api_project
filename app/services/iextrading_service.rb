# IexTrading Service is used to get Stocks info

class IextradingService
  BASE_URL = 'https://api.iextrading.com/1.0'.freeze

  def stocks_details
    url = BASE_URL + '/tops'
    JSON.parse(api_call(url))
  end

  def stock_data(stocks)
    description = []
    charts = []
    stocks.each do |stock|
      url = BASE_URL + "/stock/#{stock}/chart/1y"
      description << description(stock)
      charts << JSON.parse(api_call(url))
    end
    { charts: charts, description: description, stocks: stocks }
  end

  def description(stock)
    url = BASE_URL + "/stock/#{stock}/company"
    response = JSON.parse(api_call(url))
    response['description']
  end

  def api_call(url)
    response = Typhoeus::Request.new(url).run
    response.response_body
  end
end
