class ChangeForeignKey < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :histories, :stocks, on_delete: :cascade
  end
end
