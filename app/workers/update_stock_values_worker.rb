class UpdateStockValuesWorker
  include Sidekiq::Worker

  def perform(params)
    stocks = Stock.which_not_exists?(params)
    return if stocks.empty?

    data = IextradingService.new.stock_data(stocks)
    desc = data[:description]
    charts = data[:charts]

    Stock.new.create_history(desc, stocks, charts)
  end
end
