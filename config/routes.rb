require 'sidekiq/web'

Sidekiq::Web.set :session_secret, Rails.application.secrets.secret_key_base

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web => '/sidekiq'

  post 'authenticate', to: 'authentication#authenticate'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :stocks do
        post 'update', on: :collection
        delete 'remove', on: :collection
        get 'details', on: :collection
        get 'histories', on: :collection
      end
    end
  end
end
