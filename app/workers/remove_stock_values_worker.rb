class RemoveStockValuesWorker
  include Sidekiq::Worker

  def perform(stocks)
    Stock.new.delete(stocks)
  end
end
