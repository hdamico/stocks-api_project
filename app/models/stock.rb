# frozen_string_literal: true

class Stock < ApplicationRecord
  has_many :histories, dependent: :destroy
  validates_presence_of :name, :description

  def create_history(desc, stocks, charts)
    stock_data = []

    stocks.each_with_index do |stock, idx|
      stock = Stock.new(name: stock, description: desc[idx])
      charts[idx].each do |chart|
        stock.histories.build(date: chart['date'], value: chart['close'])
      end
      stock_data << stock
    end

    Stock.import stock_data, recursive: true
  end

  def delete(stocks)
    Stock.where(name: stocks).delete_all
  end

  def query(params)
    json_data = {}
    filters = process_filters(params)
    stocks = Stock.where(name: params[:stocks])
                  .eager_load(:histories)
                  .where('Histories.date BETWEEN ? and ?', filters[:date_from], filters[:date_to])

    stocks.each do |stock|
      json_data[stock[:name]] = stock.histories.paginate(page: filters[:page], per_page: filters[:per_page])
    end
    json_data
  end

  def details
    stocks = IextradingService.new.stocks_details
    process_existence(stocks)
  end

  def self.which_not_exists?(stocks)
    db_stocks = Stock.where(name: stocks)
    db_stocks.each do |db_stock|
      stocks.delete(db_stock.name)
    end
    stocks
  end

  private

  def process_existence(stocks)
    db_stocks = Stock.select(:name).map(&:name)
    stocks.each_with_index do |stock, idx|
      next stocks[idx]['exists'] = true if db_stocks.include? stock['symbol']

      stocks[idx]['exists'] = false
    end
    stocks
  end

  def process_filters(params)
    date_from = params[:filters][:dateFrom]
    date_to = params[:filters][:dateTo]
    page = params[:filters][:page]
    per_page = params[:filters][:perPage]

    date_from.nil? ? date_from = Time.parse(Time.current.year.to_s + '-01-01') : date_from = Time.parse(date_from)
    date_to.nil? ? date_to = Time.parse(Time.current.year.to_s + '-12-31') : date_to = Time.parse(date_to)

    page = 1 if page.nil?
    per_page = 100 if per_page.nil?

    { date_from: date_from, date_to: date_to, per_page: per_page, page: page }
  end
end
